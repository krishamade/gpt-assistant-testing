const Discord = require("discord.js");
const client = require("./client");
const { addMessage, checkingStatus } = require('./assistant.js');
const { generateImage } = require('./imageGeneration.js');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const axios = require('axios');

const commands = [
    {
        name: 'generate-image',
        description: 'Generate an image using DALL-E 3',
        options: [
            {
                name: 'description',
                type: 3, // STRING type
                description: 'The description for image generation',
                required: true
            }
        ]
    }
];

async function registerCommands() {
    const rest = new REST({ version: '9' }).setToken(process.env.DISCORD_BOT_TOKEN);

    try {
        console.log('Started refreshing application (/) commands.');

        await rest.put(
            Routes.applicationCommands(client.user.id),
            { body: commands },
        );

        console.log('Successfully reloaded application (/) commands.');
    } catch (error) {
        console.error('Error registering commands:', error);
    }
}

async function handleMessage(message) {
    let nickname = message.guild ? (message.member ? message.member.nickname || message.author.username : message.author.username) : message.author.username;
    let channelId = message.channel.id;

    // Ignore messages from other bots
    if (message.author.bot) return;

    // Skip if the bot is not mentioned or if other mentions are present
    if (
        !(message.channel instanceof Discord.DMChannel) &&
        (!message.mentions.has(client.user.id) || message.mentions.everyone || message.mentions.roles.size > 0 || message.mentions.users.size > 1)
    )
        return;

    try {
        await message.channel.sendTyping();

        const { threadId, runId } = await addMessage(message.content);
        let statusResult;
        do {
            statusResult = await checkingStatus(threadId, runId);
            await new Promise(resolve => setTimeout(resolve, 1000));
        } while (statusResult.status !== 'completed');

        if (statusResult.messages && statusResult.messages.length > 0) {
            const responseText = statusResult.messages[0];
            const MAX_MESSAGE_LENGTH = 2000;
            if (responseText.length > MAX_MESSAGE_LENGTH) {
                let messageChunks = splitIntoChunks(responseText, MAX_MESSAGE_LENGTH);
                for (const chunk of messageChunks) {
                    await message.channel.send(chunk);
                }
            } else {
                await message.reply(responseText);
            }
        } else {
            await message.reply("I'm sorry, I couldn't generate a response.");
        }
    } catch (error) {
        console.error('Error handling message:', error);
        await message.reply("I'm sorry, an error occurred while processing your message.");
    }
}

async function handleInteraction(interaction) {
    if (!interaction.isCommand()) return;

    if (interaction.commandName === 'generate-image') {
        const description = interaction.options.getString('description');

        await interaction.deferReply();

        try {
            const { imageUrls, eta } = await generateImage(description);

            if (imageUrls.length === 0) {
                await interaction.editReply("I'm sorry, I couldn't generate an image. Please try again.");
                return;
            }

            // Add a delay if there's an ETA provided
            setTimeout(async () => {
                try {
                    const responses = await Promise.all(imageUrls.map(url => axios.get(url, { responseType: 'arraybuffer' })));
                    const attachments = responses.map(response =>
                        new Discord.AttachmentBuilder(response.data)
                            .setName('image.jpg')
                            .setDescription('Generated image')
                    );
                    await interaction.editReply({ content: `Here's the generated image for: ${description}`, files: attachments });
                } catch (error) {
                    console.error('Error fetching generated images:', error);
                    await interaction.editReply('Failed to get the generated images. Please try again.');
                }
            }, eta * 1000);  // Convert ETA to milliseconds for setTimeout

        } catch (error) {
            console.error('Error generating image:', error);
            await interaction.editReply("I'm sorry, an error occurred while generating the image. Please try again later.");
        }
    }
}

function clientLogin() {
    client.once('ready', () => {
        console.log('Bot is online!');
        registerCommands();
    });

    client.on('messageCreate', handleMessage);
    client.on('interactionCreate', handleInteraction);

    client.login(process.env.DISCORD_BOT_TOKEN);
}

/**
 * Splits a string into chunks up to a specified max length.
 * @param {string} str - The string to split.
 * @param {number} maxLength - The maximum length of each chunk.
 * @returns {string[]} An array of string chunks.
 */
function splitIntoChunks(str, maxLength) {
    let chunks = [];
    while (str.length > 0) {
        let chunkSize = Math.min(str.length, maxLength);
        let chunk = str.substring(0, chunkSize);
        chunks.push(chunk);
        // Ensure we move to the next piece of the string
        str = str.substring(chunkSize);
    }
    return chunks;
}

module.exports = {
    clientLogin
};