const OpenAI = require("openai");

const openai = new OpenAI({ apiKey: process.env.OPENAI_API_KEY });

const assistantId = "asst_e6TS6Jrcpk6fXiC7TCB7YfiV";

async function createThread() {
    console.log("Creating a new thread...");
    const thread = await openai.beta.threads.create();
    console.log("Thread created:", thread.id);
    return thread.id;
}

async function runAssistant(threadId) {
    console.log("Running assistant for thread:", threadId);
    const run = await openai.beta.threads.runs.create(threadId, {
        assistant_id: assistantId,
    });
    console.log("Run created:", run.id);
    return run.id;
}

async function checkingStatus(threadId, runId) {
    console.log(`Checking status for thread: ${threadId}, run: ${runId}`);
    const runObject = await openai.beta.threads.runs.retrieve(threadId, runId);
    const status = runObject.status;
    console.log("Current status:", status);

    if (status === "completed") {
        const messagesList = await openai.beta.threads.messages.list(threadId);
        const messages = messagesList.data.map(message =>
            message.content.map(content => content.text.value).join('\n')
        );
        return { status, messages };
    }

    return { status };
}

async function addMessage(message) {
    const threadId = await createThread();
    console.log("Adding a new message to thread:", threadId);
    await openai.beta.threads.messages.create(threadId, {
        role: "user",
        content: message,
    });
    const runId = await runAssistant(threadId);
    return { threadId, runId };
}

async function generateImage(prompt) {
    console.log("Generating image with prompt:", prompt);
    try {
        const response = await openai.images.generate({
            model: "dall-e-3",
            prompt: prompt,
            n: 1,
            size: "1024x1024",
        });
        console.log("Image generated successfully");
        return response.data[0].url;
    } catch (error) {
        console.error("Error generating image:", error);
        throw error;
    }
}

module.exports = {
    addMessage,
    checkingStatus,
    generateImage
};